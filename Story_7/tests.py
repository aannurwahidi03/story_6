from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import index

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import time
import unittest
import os

# Create your tests here.
class ReportsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(apps.get_app_config('Story_7').name, 'Story_7')

class UnitTest(TestCase):
    def test_if_index_url_is_exist(self):
        response = Client().get('/Story_7/')
        self.assertEquals(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/Story_7/')
        self.assertEquals(found.func, index)

    def test_if_using_template(self):
        response = Client().get('/Story_7/')
        self.assertTemplateUsed(response, 'Story_7.html')

    def test_if_page_does_not_exist(self):
        response = Client().get('/Test/')
        self.assertEqual(response.status_code, 404)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_change_color_button(self):
        browser = self.browser
        browser.get(self.live_server_url+"/Story_7/")

        browser.find_element_by_id('change-theme').click()
        time.sleep(4)
        color = browser.find_element_by_tag_name('body').value_of_css_property("color")
        self.assertEqual("rgba(202, 202, 202, 1)", color)

        browser.find_element_by_id('change-theme').click()
        color = browser.find_element_by_tag_name('body').value_of_css_property("color")
        self.assertEqual("rgba(255, 255, 255, 1)", color)
        