from django.urls import path
from .views import index, create_comments, del_comment

urlpatterns = [
    path('', index, name='landing_page'),
    path('create_comments/', create_comments, name = 'create_comments'),
    path('del_comment/<int:id>', del_comment, name = 'del_comment'),
]