from django.shortcuts import render
from .forms import Forms_Komentar
from .models import Komentar
from django.http import HttpResponseRedirect
from django.utils import timezone
import datetime

# Create your views here.
def index(request):
    response = {"form_komentar": Forms_Komentar}
    statuss = Komentar.objects.all().values()
    response['statuss'] = statuss
    return render(request, 'landing_page.html', response)

def create_comments(request):
    if(request.method == 'POST'):
        form = Forms_Komentar(request.POST)
        if (form.is_valid()):
            all_comments = form.cleaned_data['status']
            status = Komentar(status = all_comments)
            status.save()

    return HttpResponseRedirect('/')

def del_comment(request, id):
    try:
        komentar = Komentar.objects.get(id = id)
        komentar.delete()
    except:
        pass

    return HttpResponseRedirect('/')
