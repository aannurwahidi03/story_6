from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import index, create_comments, del_comment
from .models import Komentar
from .forms import Forms_Komentar

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import time
import unittest
import os

# Create your tests here.

class ReportsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(apps.get_app_config('landing_page').name, 'landing_page')

class UntitTest(TestCase):

    def test_if_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/')
        self.assertEquals(found.func, index)

    def test_if_using_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing_page.html')

    def test_template_contains_Halo_apa_kabar(self):
        response = Client().get('/')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_form_is_valid(self):
        form_data = {'status':'Sebuah Komentar'}
        form = Forms_Komentar(data = form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['status'], 'Sebuah Komentar')

    def test_page_does_not_have_komentar(self):
        response = Client().get('/')
        self.assertContains(response, 'Belum ada komentar...')

    def test_create_comment_using_func(self):
        found = resolve('/create_comments/')
        self.assertEquals(found.func, create_comments)

    def test_del_comment_using_func(self):
        found = resolve('/del_comment/1')
        self.assertEquals(found.func, del_comment)

class ModelsTest(TestCase):
        
    def test_if_komentar_created(self):
        komentar = Komentar(status = "Test 1")
        komentar.save()
        count = Komentar.objects.all().count()
        self.assertEqual(count, 1)

    def test_if_komentar_max_lenth_is_300(self):
        komentar_length = Komentar._meta.get_field('status').max_length
        self.assertEqual(komentar_length, 300)

class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options=chrome_options)
        super(NewVisitorTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(NewVisitorTest, self).tearDown()

    def test_can_start_a_list_and_retrieve_it_later(self):
        browser = self.browser
        browser.get(self.live_server_url+"/")
        # self.browser.get('localhost:8000/')
        # time.sleep(5) # Let the user actually see something!
        search_box = browser.find_element_by_id('id_status')
        search_box.send_keys('PPW')
        search_box.submit()
        # time.sleep(5) # Let the user actually see something!
        self.assertIn("PPW", browser.page_source)