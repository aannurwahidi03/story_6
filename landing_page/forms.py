from django import forms
from django.forms import ModelForm
from django.db import models
from .models import Komentar

class Forms_Komentar(ModelForm):
    class Meta:
        model = Komentar
        fields = "__all__"
        exclude = ('date',)   
