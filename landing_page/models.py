from django.db import models
from django.utils import timezone

# Create your models here.
class Komentar(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now=True)